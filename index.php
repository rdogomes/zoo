<?php
session_start();

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 18:07
 */

require_once 'system/config/contants.php';
require_once 'system/controller/controller.php';
$myController = new Controller();

// Método que redireciona para cada página solicitada
$myController->router();