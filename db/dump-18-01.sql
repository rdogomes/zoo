-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: zoo
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(155) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `secao` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categoria_secao1_idx` (`secao`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Decor',1,'1'),(2,'Storage & Cleaning',1,'1'),(3,'Craft & Sewing',1,'1'),(4,'Bar',1,'1'),(5,'Bags',1,'2,7,8'),(6,'Wear',1,'2'),(7,'Coffee & Tea',1,'3'),(8,'Cooking & Baking',1,'3'),(9,'Cutlery & Cutting',1,'3'),(10,'Serving',1,'3'),(11,'Food Storage',1,'3'),(12,'Writing Instruments',1,'4'),(13,'Shaving',1,'5'),(14,'Hair Care',1,'5'),(15,'Bath & Body',1,'5'),(16,'Tools',1,'6'),(17,'Camp Gear',1,'6'),(18,'Bike, Sports & Play',1,'6'),(19,'Jackets',1,'7,8,9'),(20,'Blazers',1,'7,8,9'),(21,'Suits',1,'7,8,9'),(22,'Trousers',1,'7,8,9'),(23,'Shirts',1,'7,8,9'),(24,'Jeans',1,'7,8,9');
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca`
--

DROP TABLE IF EXISTS `marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(155) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `secao` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca`
--

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` VALUES (1,'Nike',1,NULL),(2,'Dolce & Gabbana',1,NULL),(3,'Dsquared2',1,NULL),(4,'Equipment',1,NULL),(5,'Gucci',1,NULL),(6,'Liu Jo',1,NULL),(7,'Liu Jo Jeans',1,NULL),(8,'Love Moschino',1,NULL),(9,'Maison Martin Magiela',1,NULL),(10,'Moschino',1,NULL);
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produto`
--

DROP TABLE IF EXISTS `produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foto` varchar(255) DEFAULT NULL,
  `nome` varchar(155) DEFAULT NULL,
  `marca` int(11) NOT NULL,
  `preco` float(8,2) DEFAULT NULL,
  `categoria` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`marca`,`categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produto`
--

LOCK TABLES `produto` WRITE;
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` VALUES (1,'1484753874.jpg','Soapstone',4,80.00,2,1),(2,'1484755879.jpg','Cashmere Sweater',2,280.00,19,1),(3,'1484757475.jpg','Cashmere Sweater 2',6,280.00,20,1),(4,'1484757513.jpg','Cashmere Sweater 3',2,290.00,19,1),(5,'1484757558.jpg','Cashmere Sweater 4',5,300.00,20,1),(6,'1484757723.jpg','Cashmere Sweater 5',6,590.00,9,1),(7,'1484757753.jpg','Calças Roupas 6',8,350.00,20,1),(8,'1484757795.jpg','Calça Jeans',3,500.90,24,1),(9,'1484757856.jpg','Calça Jeans Preta',2,150.00,24,1),(10,'1484758472.jpg','Blusa',1,280.00,19,1);
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `secao`
--

DROP TABLE IF EXISTS `secao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `secao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `status` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `secao`
--

LOCK TABLES `secao` WRITE;
/*!40000 ALTER TABLE `secao` DISABLE KEYS */;
INSERT INTO `secao` VALUES (1,'Home Decor','1'),(2,'Accessories','1'),(3,'Kitchen & Tabletop','1'),(4,'Office','1'),(5,'Home Decor 2','1'),(6,'Tools & Outdoors','1'),(7,'Women','1'),(8,'Men','1'),(9,'Kids','1');
/*!40000 ALTER TABLE `secao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-18 16:33:44
