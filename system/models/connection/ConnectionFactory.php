<?php

/**
 * Created by Rodrigo Gomes do Nascimento.*
 * Date: 13/01/2017
 * Time: 13:32
 */

class ConnectionFactory
{
    public static $pdo = '';

    public function __construct($host = 'localhost', $dbname = 'zoo', $user = 'root', $pass = '')
    {
        try{
            // Crio a conexão com o banco com PDO
            self::$pdo = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$pdo->exec('SET NAMES UTF8');
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }

    public static function instance(){
        // Crio uma nova instância se não houver uma criada.
        if(!self::$pdo){
            new ConnectionFactory();
        }

        return self::$pdo;
    }
}