<?php
/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 14:12
 */
ini_set('Display_errors' , 'On');
error_reporting(E_ALL);
require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR .'models/connection/ConnectionFactory.php';

class Model_model extends ConnectionFactory
{
    /*
     * @param $tabela String especifica qual tabela vão ser buscados os registros
     * @param $campos array dos campos a serem filtrados
     * @param $where array condição na consulta dos registros
     * @param $groupBy array quais campos vão ser agrupados
     * @param $orderBy array quais campos vão ser ordenados
     * @param $limit limita a quantidade de registros
     * @return array com os registros encontrados
     */
    public function select($tabela, $campos = '*', $where = null, $groupBy = null, $orderBy = null, $limit = null)
    {
        $condicao = ' AND ';

        // Verifico se campos veio com dados do controller. Se não permanece *
        if(is_array($campos) && count($campos) > 0):
            $campos = implode(',', $campos);
        endif;

        // Verifico se orderBy veio com dados do controller. Se não permanece null
        if(is_array($groupBy) && count($groupBy) > 0):
            $groupBy = ' GROUP BY ' .implode(',', $groupBy);
        endif;

        // Verifico se groupBy veio com dados do controller. Se não permanece null
        if(is_array($orderBy) && count($orderBy) > 0):
            $orderBy = ' ORDER BY ' .implode(',', $orderBy);
        endif;

        // Verifico se $limit veio com dados do controller. Se não permanece null
        if(is_array($limit) && count($limit) > 0):
            $i = 0;
            $limit = ' LIMIT ';
            foreach($limit as $key => $value):
                $i++;
                $limit .= $value.((count($limit) == $i) ? '' : ',' );
            endforeach;
        endif;

        // Verifico se $where veio do controller. Se sim e não estiver vazio, percorro o mesmo.
        if(is_array($where) && count($where) > 0):
            $i = 0;
            foreach($where as $key => $value):
                $i++;
                $condicao .= $key . ' IN( :' . $key.')'.((count($where) == $i) ? '' : ' AND ' );
            endforeach;
        else:
            $condicao = '';
        endif;

        try {
            // Preparo a consulta
            $stmt = self::$pdo->prepare("
                SELECT 
                    $campos
                FROM
                    $tabela
                WHERE
                    1 = 1 
                    $condicao
                $groupBy
                $orderBy
                $limit                
            ");

            // Verifico se $where veio do controller. Se sim e não estiver vazio, percorro o mesmo.
            if(is_array($where) && count($where) > 0):
                foreach($where as $key => $value):
                    $stmt->bindValue(':'.$key, $value);
                endforeach;
            endif;

            // Verifico se executou com sucesso. Se sim retorno um array com os registros
            if($stmt->execute()):
                return $stmt->fetchAll(PDO::FETCH_OBJ);
            endif;
        }catch(Exception $e) {
            echo "Erro ao buscar registros: " . $e->getMessage();
        }

        return FALSE;
    }

    /*
     * @param $sql String com a query a ser executada
     */
    public function query($sql)
    {
        try{
            $stmt = self::$pdo->query($sql);
            if($stmt->rowCount() > 0):
                return $stmt->fetchAll(PDO::FETCH_OBJ);
            endif;
        }catch (PDOException $e){
            echo "Erro ao inserir registro: " . $e->getMessage();
        }

        return FALSE;
    }

    /*
     * @param $tabela Array especifica qual tabela vão ser inseridos os registros
     * @param $campos Array dos campos a serem inseridos os valores
     * @param $values Array valores a serem inseridos
     */
    public function insert($tabela, $data)
    {
        $novosCampos = '';

        $i = 0;
        // Verifico se campos veio com dados do controller.
        if(is_array($data) && count($data) > 0):
            foreach ($data as $key => $value):
                $i++;
                $novosCampos .= $key.' = :'.$key.((count($data) == $i) ? '' : ',' );
            endforeach;
        endif;


            $stmt = self::$pdo->prepare(
                "INSERT INTO
                    $tabela
                SET
                    $novosCampos"
            );

            // Verifico se campos veio com dados do controller.
            if(is_array($data) && count($data) > 0):
                foreach ($data as $key => $value):
                    $stmt->bindValue(':'.$key, $value);
                endforeach;
            endif;

        try{
            $stmt->execute();
            return self::$pdo->lastInsertId();
        }catch (PDOException $e){
            echo "Erro ao inserir registro: " . $e->getMessage();
        }
        exit();
        return FALSE;
    }

    /*
     * @param $tabela Array especifica qual tabela vão ser atualizados os registros
     * @param $campos Array dos campos a serem atualizados os valores
     * @param $values Array valores a serem atualizados
     */
    public function update($tabela, $data, $id)
    {
        $novosCampos = '';
        $i = 0;
        // Verifico se campos veio com dados do controller.
        if(is_array($data) && count($data) > 0):
            foreach ($data as $key => $value):
                $i++;
                $novosCampos .= $key.' = :'.$key.((count($data) == $i) ? '' : ',' ) ;
            endforeach;
        endif;

        try{
            $stmt = self::$pdo->prepare(
                "UPDATE
                    $tabela
                SET
                    $novosCampos
                WHERE
                    id = :id"
            );

            // Verifico se campos veio com dados do controller.
            if(is_array($data) && count($data) > 0):
                foreach ($data as $key => $value):
                    $stmt->bindValue(':'.$key, $value);
                endforeach;
            endif;
            $stmt->bindValue(':id', $id);

            if($stmt->execute()):
                return TRUE;
            endif;
        }catch (PDOException $e){
            echo "Erro ao inserir registro: " . $e->getMessage();
        }

        return FALSE;
    }


    /*
     * @param $tabela Array especifica qual tabela vão ser excluido os registros
     * @param $where Array com a condição a ser executada na exclusao
     * @return TRUE se executar com sucesso e FALSE caso contrário
     */
    public function delete($tabela, $where){
        $condicao = '';

        // Verifico se $where veio do controller. Se sim e não estiver vazio, percorro o mesmo.
        if(is_array($where) && count($where) > 0):
            foreach($where as $key => $value):
                $condicao .= ' AND ' .$key . ' = :' . $key;
            endforeach;
        else:
            $condicao = '';
        endif;

        $stmt = self::$pdo->prepare("
            DELETE FROM 
                $tabela
            WHERE 
                1 = 1 
            $condicao
        ");
        // Verifico se $where veio do controller. Se sim e não estiver vazio, percorro o mesmo.
        if(is_array($where) && count($where) > 0):
            foreach($where as $key => $value):
                $stmt->bindValue(':'.$key, $value);
            endforeach;
        endif;

        // se executou retorno true
        if($stmt->execute()):
            return TRUE;
        endif;

        return FALSE;
    }

}