<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:30
 */


class Controller
{
    /*
     * inclui a view passada em $view
     */
    public function loadView($view, $data = null)
    {
        if(is_array($data) && count($data) > 0):
            foreach ($data as $key => $value):
                $$key = $value;
            endforeach;
        endif;
        include APP_FOLDER.DIRECTORY_SEPARATOR.$view.'.php';
    }

    /*
     * inclui a model passada em $model
     */
    public function loadModel($model)
    {
        include APP_FOLDER.DIRECTORY_SEPARATOR.$model.'_model.php';
    }

    /*
     * redireciona para a url passada em $url
     */
    public function redirect($url){
        if(!empty($url)):
            header('location:'.$url);
        endif;
    }

    /*
     * Método responsável por criar as rotas do framework
     */
    public function router(){
        $controller = '';
        $action     = '';
        $path       = '';

        $uri = urldecode($_SERVER['REQUEST_URI']);
        $url = explode('/', $uri);

        if(count($url) > 0){
            if($url[2] == 'admin'){
                $path       = $url[2].DIRECTORY_SEPARATOR;
                $controller = (isset($url[3]) && $url[3] != '') ? $url[3] : 'dashboard';
                $action     = (isset($url[4]) && $url[4] != '') ? $url[4] : 'index';
                $id         = (isset($url[5]) && $url[5] != '') ? $url[5] : '';
            } elseif($url[2] == 'm'){
                $path       = 'site'.DIRECTORY_SEPARATOR;
                $controller  = 'home';
                $action      = 'brand';
                $id = array(
                    'marca'         => $url[3],
                    'page'          => (isset($url[4]) && $url[4] != '') ? $url[4] : '1',
                );
            }elseif($url[2] == 'c'){
                $path           = 'site'.DIRECTORY_SEPARATOR;
                $controller     = 'home';
                $action         = 'category';
                $id = array(
                    'categoria'   => $url[3],
                    'page'          => (isset($url[4]) && $url[4] != '') ? $url[4] : '1',
                );
            }else{
                $path        = 'site/';
                $controller  = (isset($url[2]) && $url[2] != '') ? $url[2] : 'home';
                $action      = (isset($url[3]) && $url[3] != '') ? $url[3] : 'index';
                $id          = array(
                    'page' => (isset($url[4]) && $url[4] != '') ? $url[4] : '1'
                );
            }
        }else{
            $controller = 'home';
            $action     = 'index';
            $id = array(
                'page' => (isset($url[4]) && $url[4] != '') ? $url[4] : '1'
            );
        }

        include APP_FOLDER.DIRECTORY_SEPARATOR.$path.'controller'.DIRECTORY_SEPARATOR.$controller.'.php';

        $c = new $controller();

        $c->$action(isset($id) && $id != '' ? $id : NULL);
    }

    /*
     * Método responsável por criar as mensagem de retorno
     */
    public function alert($msg, $type){
        return '<div class="alert alert-'.(($type != '') ? $type : 'success').'" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>'.
                $msg.
            '</div>';
    }

    /*
     * Método responsável por validar os campos
     */
    public function validation($data){
        // Valido a variavel $_POST
        foreach ($data as $key => $val):
            if($val == ''):
                return FALSE;
            endif;
        endforeach;

        return TRUE;
    }

    /*
     * Método resposável por fazer upload de imagens
     */
    public function uploadImage($file, $path){
        // Pasta onde o arquivo vai ser salvo
        $_UP['pasta'] = $path;

        // Tamanho máximo do arquivo (em Bytes)
        $_UP['tamanho'] = 3024 * 3024 * 2; // 2Mb

        // Array com as extensões permitidas
        $_UP['extensoes'] = array('jpg', 'png', 'gif');

        // Renomeia o arquivo? (Se true, o arquivo será salvo como .jpg e um nome único)
        $_UP['renomeia'] = true;

        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
        $_UP['erros'][4] = 'Não foi feito o upload do arquivo';

        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($file['error'] != 0) {
            die("Não foi possível fazer o upload, erro:<br />" . $_UP['erros'][$file['error']]);
            return false;
            exit; // Para a execução do script
        }

        // Caso script chegue a esse ponto, não houve erro com o upload e o PHP pode continuar
        // Faz a verificação da extensão do arquivo
        $extensao = strtolower(end(explode('.', $file['name'])));
        if (array_search($extensao, $_UP['extensoes']) === false) {
            echo "Por favor, envie arquivos com as seguintes extensões: jpg, png ou gif";
        }

        // Faz a verificação do tamanho do arquivo
        else if ($_UP['tamanho'] < $file['size']) {
            echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
        }

        // O arquivo passou em todas as verificações, hora de tentar movê-lo para a pasta
        else {
            // Primeiro verifica se deve trocar o nome do arquivo
            if ($_UP['renomeia'] == true) {
                // Cria um nome baseado no UNIX TIMESTAMP atual e com extensão .jpg
                $nome_final = time().'.jpg';
            } else {
                // Mantém o nome original do arquivo
                $nome_final = $file['name'];
            }

            // Depois verifica se é possível mover o arquivo para a pasta escolhida
            if (move_uploaded_file($file['tmp_name'], $_UP['pasta'] . $nome_final)) {
                // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
                echo "Upload efetuado com sucesso!";
                return $nome_final;
            } else {
                // Não foi possível fazer o upload, provavelmente a pasta está incorreta
                $_SESSION['msg'] = "Não foi possível enviar o arquivo, tente novamente";
                return false;
            }
        }

    }

    /**
     * Paginação
     *
     * Cria uma paginação simples.
     *
     * @param int $total_artigos Número total de artigos da sua consulta
     * @param int $artigos_por_pagina Número de artigos a serem exibidos nas páginas
     * @param int $offset Número de páginas a serem exibidas para o usuário
     *
     * @return string A paginação montada
     */
    function pagination($limit = 9, $page = 0, $count = 0, $router) {
        $pags = ceil ( $count / $limit );
        $anterior = $page - 1;
        $proximo = $page + 1;

        $pagination = '';
        if ($count > $limit) {
            $pagination .= "<div class='row'>";
            $pagination .= "     <div class='col-lg-12 col-md-12 col-sm-12'>";
            $pagination .= "          <div class='pagination'>";
            $pagination .= "	<ul class='pagination-sm'>";

            if ($anterior > 0) {
                $pagination .= "<li>";
                $pagination .= "	<a href='" . URL_SITE . "$router" . "$anterior/' aria-label='Previous'>";
                $pagination .= "		<span aria-hidden='true'>&laquo;</span>";
                $pagination .= "	</a>";
                $pagination .= "</li>";
            } else {
                $pagination .= "<li class='disabled'>";
                $pagination .= "	<span aria-hidden='true'>&laquo;</span>";
                $pagination .= "</li>";
            }


            for ($i = 1; $i <= $pags; $i++) {
                if ($i < ($page - 4) && $i == 1) {
                    $pagination .= "<li><a href='" . URL_SITE . "$router" . "$i/'>" . $i . "</a></li>";
                    $pagination .= "<li><a>...</a></li>";
                }

                if ($i >= ($page - 4) && $i <= ($page + 4)) {
                    if ($i == $page) {
                        $pagination .= "<li class='active'><a href='" . URL_SITE . "$router" . "$i/'>" . $i . "</a></li>";
                    } else {
                        $pagination .= "<li><a href='" . URL_SITE . "$router" . "$i/'>" . $i . "</a></li>";
                    }
                }

                if ($i > ($page + 4) && $i == $count) {
                    $pagination .= "<li><a>...</a></li>";
                    $pagination .= "<li><a href='" . URL_SITE . "$router" . "$i/'>" . $i . "</a></li>";
                }
            }

            if ($page < $pags) {
                $pagination .= "<li>";
                $pagination .= "	<a href='" . URL_SITE . "$router" . "$proximo/' aria-label='Next'>";
                $pagination .= "		<span aria-hidden='true'>&raquo;</span>";
                $pagination .= "	</a>";
                $pagination .= "</li>";
            } else {
                $pagination .= "<li class='disabled'>";
                $pagination .= "	<span aria-hidden='true'>&raquo;</span>";
                $pagination .= "</li>";
            }

            $pagination .= "  </ul>";
            $pagination .= "                </div>";
            $pagination .= "            </div>";
            $pagination .= "        </div>";
           // $pagination .= "</nav>";
        }

        // Retorna o que foi criado
        return $pagination;

    }
}