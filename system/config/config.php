<?php
/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 16/01/2017
 * Time: 11:02
 */

$config = array(
    'db' => array(
        'host'      => 'localhost',
        'user'      => 'root',
        'pass'      => '',
        'dbname'    => 'zoo'
    ),
    'url' => array(
        'base_site' => 'http://'.$_SERVER['HTTP_HOST'].'/zoo/',
    )
);