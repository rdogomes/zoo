<?php require_once 'default/header.php'; ?>

            <!-- START SLIDE BANNER -->
            <div class="row">
                <div class="col-md-12 hidden-sm-down">
                    <div class="slide-banner carousel slide" id="slide-banner" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slide-banner" data-slide-to="0" class="active"></li>
                            <li data-target="#slide-banner" data-slide-to="1"></li>
                            <li data-target="#slide-banner" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img src="<?= URL_SITE; ?>assets/img/banner.png" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>For Him</h3>
                                    <p>
                                        Explore our most popular picks from the <br>
                                        land of the rising sun (and beyond)
                                    </p>
                                    <a href="#" class="btn btn-primary" title="">SHOP NOW</a>
                                </div>
                            </div>

                            <div class="carousel-item">
                                <img src="<?= URL_SITE; ?>assets/img/banner.png" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>For Him</h3>
                                    <p>
                                        Explore our most popular picks from the <br>
                                        land of the rising sun (and beyond)
                                    </p>
                                    <a href="#" class="btn btn-primary" title="">SHOP NOW</a>
                                </div>
                            </div>

                            <div class="carousel-item">
                                <img src="<?= URL_SITE; ?>assets/img/banner.png" alt="...">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>For Him</h3>
                                    <p>
                                        Explore our most popular picks from the <br>
                                        land of the rising sun (and beyond)
                                    </p>
                                    <a href="#" class="btn btn-primary" title="">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <!-- END SLIDE BANNER -->

            <!-- FEATURED PRODUCTS -->
            <div class="row">
                <div class="col-md-12 hidden-sm-down">
                <div class="featured-products">
                    <div class="col-md-4">
                        <div class="item">
                            <div class="picture">
                                <img src="<?= URL_SITE; ?>assets/img/products/le-parfait.png">
                            </div>

                            <div class="caption">
                                <h3 class="category">Food Storage</h3>
                                <h5 class="name">Le Parfait Canning</h5>
                                <p class="price">$ 13.34</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="item">
                            <div class="picture">
                                <img src="<?= URL_SITE; ?>assets/img/products/soapstone.png">
                            </div>
                            <div class="caption">
                                <h3 class="category">Cooking & Back</h3>
                                <h5 class="name">SoapStone & Copper</h5>
                                <p class="price">$ 198.54</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="item">
                            <div class="picture">
                                <img src="<?= URL_SITE; ?>assets/img/products/poplar.png">
                            </div>
                            <div class="caption">
                                <h3 class="category">Home Decore</h3>
                                <h5 class="name">Poplar Wood</h5>
                                <p class="price">$ 39.00</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>
            <!-- END FEATURED PRODUCTS -->
            <div class="clearfix"></div>

            <div class="row">
                <!-- START SIDEBAR -->
                <div class="col-lg-3 col-md-3 col-sm-12" id="sidebar">
                    <aside>
                        <div class="sidebar-left">
                            <!-- BRANDS -->
                            <div class="brands">
                                <h1>BRANDS</h1>
                                <form action="busca" method="post">
                                    <input type="search" name="search" placeholder="Search Brand..." class="form-control input-search">
                                    <button type="submit" class="btn-search"><i class="fa fa-search"></i></button>
                                </form>

                                <ul class="menu-brands">
                                    <?php if(isset($brands) && count($brands) > 0): ?>
                                        <?php foreach ($brands as $val): ?>
                                            <li <?= (isset($filter['marca']) && $filter['marca'] == $val->id ) ? 'class="active"' : ''; ?>><a href="<?= URL_SITE; ?>m/<?= $val->id; ?>/" <?= (isset($filter['marca']) && $filter['marca'] == $val->id ) ? 'class="active"' : ''; ?>><?= $val->nome; ?> <span class="total"><?= $val->total; ?></span> </a> </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                            <!-- CATEGORIES -->
                            <div class="category panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <h1 data-parent="#accordion" data-toggle="collapse" href="#women-collapse" aria-expanded="false" aria-controls="women-collapse">WOMEN <span class="plus"> + </span></h1>
                                <ul class="menu-brands panel-collapse collapse" id="women-collapse" role="tabpanel">
                                    <?php if(isset($categories['Women']) && count($categories['Women']) > 0): ?>
                                        <?php foreach ($categories['Women'] as $val): ?>
                                            <li ><a href="<?= URL_SITE; ?>c/<?= $val->id; ?>/"><?= $val->nome; ?></a> </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                                <h1 data-parent="#accordion" data-toggle="collapse" href="#men-collapse" aria-expanded="false" aria-controls="men-collapse">MEN <span class="plus"> + </span></h1>
                                <ul class="menu-brands collapse" id="men-collapse" role="tabpanel">
                                    <?php if(isset($categories['Men']) && count($categories['Men']) > 0): ?>
                                        <?php foreach ($categories['Men'] as $val): ?>
                                            <li><a href="<?= URL_SITE; ?>c/<?= $val->id; ?>/"><?= $val->nome; ?></a> </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>

                                <h1 data-parent="#accordion" data-toggle="collapse" href="#kids-collapse" aria-expanded="false" aria-controls="kids-collapse">KIDS <span class="plus"> + </span></h1>
                                <ul class="menu-brands collapse" id="kids-collapse" role="tabpanel">
                                    <?php if(isset($categories['Kids']) && count($categories['Kids']) > 0): ?>
                                        <?php foreach ($categories['Kids'] as $val): ?>
                                            <li><a href="<?= URL_SITE; ?>c/<?= $val->id; ?>/"><?= $val->nome; ?></a> </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                                <h1 data-parent="#accordion" data-toggle="collapse" href="#accessories-collapse" aria-expanded="false" aria-controls="accessories-collapse">ACCESSORIES <span class="plus" id="more-women"> + </span></h1>
                                <ul class="menu-brands collapse" id="accessories-collapse" role="tabpanel">
                                    <?php if(isset($categories['Accessories']) && count($categories['Accessories']) > 0): ?>
                                        <?php foreach ($categories['Accessories'] as $val): ?>
                                            <li><a href="<?= URL_SITE; ?>c/<?= $val->id; ?>/"><?= $val->nome; ?></a> </li>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>

                            </div>
                        </div>
                    </aside>
                </div>

                <!-- END SIDEBAR -->

                <!-- START PRODUCTS -->
                <div class="col-lg-9 col-md-9 col-sm-12" id="content">
                    <div class="row">

                        <?php if(isset($products[0]) && count(current($products)) > 0): ?>
                            <?php foreach ($products as $val): ?>
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="products">
                                        <div class="item">
                                            <div class="img-product">
                                                <img src="<?= URL_SITE; ?>assets/img/upload/<?= $val->foto?>" alt="<?= $val->nome; ?>" class="img-fluid">
                                            </div>
                                            <div class="info-product">
                                                <a href=""><?= $val->nome; ?></a><br>
                                                <p>
                                                    <span class="favorite-icon">
                                                        <img src="<?= URL_SITE; ?>assets/img/favorite.png" alt="" id="add-favorite">
                                                    </span>
                                                    $ <?= $val->preco; ?>
                                                    <span class="cart-icon" id="<?= 'prod-'.$val->id; ?>">
                                                        <img src="<?= URL_SITE; ?>assets/img/cart-product-item.png" alt="">
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>

                    </div>

                    <!-- PAGINATION -->
                    <?= $pagination; ?>
                </div>
                <!-- END PRODUCTS -->
            </div>

            <!-- START FOOTER -->
            <?php require_once 'default/footer.php'; ?>
            <!-- END FOOTER -->

        <!-- END CONTAINER -->
        </div>

        <!-- modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p id="modal-body"></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $('.carousel').carousel({
                interval: 5000
            })


            /*
             * Created by Rodrigo Gomes do Nascimento on 20/01/2017.
             */

            $(document).ready(function(){
                $(document).on('click','.cart-icon', function(){
                    var sp = $(this).attr('id').split('-');

                    // ADICIONA O PRODUTO AO CARRINHO E ATUALIZA O HTML DO MESMO
                    $.ajax({
                        type: 'POST',
                        data: {id: sp[1]},
                        url: '<?= URL_SITE; ?>home/addToCart/',
                        success: function (data) {
                            if(data != 'erro'){
                                $(".modal-title").text('Sucesso');
                                $(".modal-body").text('Produto adicionado com sucesso!');
                                $("#myModal").modal('show');
                                $('#cart-collapse .col-md-12').html(data);

                            }
                        }
                    });

                    $('.cart a.dropdown span.count-cart').text($('#cart-collapse.panel-collapse.collapse.show div.col-md-12 div.row.line').length);
                });

                // REMOVE O PRODUTO DO CARRINHO E ATUALIZA O HTML DO MESMO
                $(document).on('click','.remove-cart', function(e){
                    e.preventDefault();
                    var sp = $(this).attr('id').split('-');

                    $.ajax({
                        type: 'POST',
                        data: {id: sp[1]},
                        url: '<?= URL_SITE; ?>home/removeToCart/',
                        success: function (data) {
                            if(data != 'erro'){
                                //$(".modal-title").text('Sucesso');
                                //$(".modal-body").text('Produto removido com sucesso!');
                                //$("#myModal").modal('show');
                                $('#cart-collapse .col-md-12').html(data);
                                $('.cart a.dropdown span.count-cart').text($('#cart-collapse.panel-collapse.collapse.show div.col-md-12 div.row.line').length);
                            }
                        }
                    });
                });
                if($(window).width() <= 991){
                    /* to toggle the sidebar, just switch the CSS classes */
                    $(".toggle-sidebar").click(function () {
                        $("#sidebar").toggleClass("collapsed-sidebar");
                        $("#sidebar").toggleClass("col-md-3 col-md-12");
                        $("#content").toggleClass("col-md-12 col-md-9");
                        return false;
                    });
                    $('#filter-btn').addClass('header-shadow')
                    $('.header').addClass('no-radius')
                    $("#sidebar").toggleClass("collapsed-sidebar");
                    $("#content").toggleClass("col-md-12 col-md-9");
                }else if($(window).width() > 991){
                    $('#filter-btn').hide();
                }


            });
        </script>


</body>
</html>
