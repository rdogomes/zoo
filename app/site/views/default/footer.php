            <div class="row">
                <div class="col-md-12">
                    <footer>
                        <div class="col-md-2 offset-1">
                            <img src="<?= URL_SITE; ?>assets/img/logo-site-footer.png" class="logo-footer" alt="Kauf" title="Kauf">
                        </div>
                        <div class="col-md-8 menu-footer">
                            <div class="row">
                                <div class="col-md-4">
                                    <h3>About Kauf Shop</h3>
                                    <ul>
                                        <li><a href="#">About</a></li>
                                        <li><a href="#">Team</a></li>
                                        <li><a href="#">Security</a></li>
                                        <li><a href="#">Fees</a></li>
                                    </ul>
                                </div>

                                <div class="col-md-4">
                                    <h3>Customer Service</h3>
                                    <ul>
                                        <li><a href="#">FAQ</a></li>
                                        <li><a href="#">Delivery</a></li>
                                        <li><a href="#">Returns</a></li>
                                        <li><a href="#">Track Order</a></li>
                                        <li><a href="#">Size Guide</a></li>
                                    </ul>
                                </div>

                                <div class="col-md-4">
                                    <h3>Get In Touch</h3>
                                    <ul>
                                        <li><a href="#">Contacts</a></li>
                                        <li><a href="#">Carees</a></li>
                                        <li><a href="#">Community</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-11">
                            <hr class="line-footer">
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-6 offset-1">
                                <div class="copy-footer">
                                    <p>Copyright &copy; 2016 Yebo Creative Team.<br>
                                        All rights reserved</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="social-footer">
                                    <a href=""><img src="<?= URL_SITE ;?>assets/img/twitter-footer.png" alt="kauf twitter" title="kauf twitter"> </a>
                                    <a href=""><img src="<?= URL_SITE ;?>assets/img/facebook-footer.png" alt="kauf facebook" title="kauf facebook"> </a>
                                    <a href=""><img src="<?= URL_SITE ;?>assets/img/gplus-footer.png" alt="kauf gplus" title="kauf gplus"> </a>
                                </div>
                            </div>
                        </div>


                        <div class="clearfix"></div>
                    </footer>
                </div>
            </div>

            <!-- Scrips includes -->
            <script src="<?= URL_SITE; ?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
            <script src="<?= URL_SITE; ?>assets/js/tether.min.js"></script>
            <script src="<?= URL_SITE; ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
            <!--<script src="<?= URL_SITE; ?>assets/js/site-script.js" type="text/javascript"></script>-->
