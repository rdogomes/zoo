<?php
/**
 * Created by PhpStorm.
 * User: rdn10
 * Date: 16/01/2017
 * Time: 16:55
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Categorias</title>
    <link href="<?=URL_SITE; ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=URL_SITE; ?>assets/css/site-style.css" rel="stylesheet">
</head>
<body>

        <!-- START CONTAINER -->
        <div class="container">
            <!-- START NAVBAR -->
            <div class="row">
                <div class="col-md-12">
                    <div class="sign-in">
                        <a class="nav-link" href="#">Sign Up </a>
                        <a class="nav-link" href="#">Sign in <span class="divide-signin">|</span></a>
                        <br>
                        <hr>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <header class="header">
                        <nav class="navbar navbar-toggleable-md navbar-light bg-primary">
                            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            <a class="navbar-brand" href="<?= URL_SITE; ?>"><img src="<?= URL_SITE; ?>assets/img/logo-site.png" class="img-fluid" alt="Kauf" title="Kauf"></a>
                            <div class="collapse navbar-collapse offset-md-1" id="navbarText">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item active dropdown">
                                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Categories</a>
                                            <div class="dropdown-menu nav-categories">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <div class="row">
                                                                <div class="col-md-12 categories">
                                                                    <h1>HOME DECOR</h1>
                                                                    <div class="clearfix"></div>
                                                                    <ul>
                                                                        <?php if(isset($categories['Home Decor']) && count($categories['Home Decor']) > 0): ?>
                                                                            <?php foreach($categories['Home Decor'] as $key => $val): ?>
                                                                                <li><a class="dropdown-item" href="#"><?= $val->nome; ?></a></li>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </ul>
                                                                </div>

                                                                <div class="col-md-12 categories">
                                                                    <h1 class="text-uppercase">ACESSORIES</h1>
                                                                    <div class="clearfix"></div>
                                                                    <ul>
                                                                        <?php if(isset($categories['Accessories']) && count($categories['Accessories']) > 0): ?>
                                                                            <?php foreach($categories['Accessories'] as $key => $val): ?>
                                                                                <li><a class="dropdown-item" href="#"><?= $val->nome; ?></a></li>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="row">
                                                                <div class="col-md-12 categories">
                                                                    <h1 class="text-uppercase">KITCHEN & TABLETOP</h1>
                                                                    <div class="clearfix"></div>
                                                                    <ul>
                                                                        <?php if(isset($categories['Kitchen & Tabletop']) && count($categories['Kitchen & Tabletop']) > 0): ?>
                                                                            <?php foreach($categories['Kitchen & Tabletop'] as $key => $val): ?>
                                                                                <li><a class="dropdown-item" href="#"><?= $val->nome; ?></a></li>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-12 categories">
                                                                    <h1 class="text-uppercase">OFFICE</h1>
                                                                    <div class="clearfix"></div>
                                                                    <ul>
                                                                        <?php if(isset($categories['Office']) && count($categories['Office']) > 0): ?>
                                                                            <?php foreach($categories['Office'] as $key => $val): ?>
                                                                                <li><a class="dropdown-item" href="#"><?= $val->nome; ?></a></li>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="row">
                                                                <div class="col-md-12 categories">
                                                                    <h1 class="text-uppercase">HOME & DECOR</h1>
                                                                    <div class="clearfix"></div>
                                                                    <ul>
                                                                        <?php if(isset($categories['Home Decor']) && count($categories['Home Decor']) > 0): ?>
                                                                            <?php foreach($categories['Home Decor'] as $key => $val): ?>
                                                                                <li><a class="dropdown-item" href="#"><?= $val->nome; ?></a></li>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-md-12 categories">
                                                                    <h1 class="text-uppercase">TOOLS & OUTDOORS</h1>
                                                                    <div class="clearfix"></div>
                                                                    <ul>
                                                                        <?php if(isset($categories['Tools & Outdoors']) && count($categories['Tools & Outdoors']) > 0): ?>
                                                                            <?php foreach($categories['Tools & Outdoors'] as $key => $val): ?>
                                                                                <li><a class="dropdown-item" href="#"><?= $val->nome; ?></a></li>
                                                                            <?php endforeach; ?>
                                                                        <?php endif; ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="row">
                                                                <div class="col-md-12 categories-product">
                                                                    <img src="<?= URL_SITE; ?>assets/img/product-hover.png" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Shop</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">For Sale</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#"><img src="<?= URL_SITE; ?>assets/img/search-navbar-top.png" alt="" title=""> </a>
                                    </li>
                                </ul>
                            </div>


                        </nav>
                        <div class="cart">
                            <a class="dropdown" class="dropdown-toggle" data-toggle="collapse" href="#cart-collapse" aria-expanded="false" aria-controls="cart-collapse">
                                <img src="<?= URL_SITE; ?>assets/img/cart.png" alt="" title=""> <span class="legend"> CART </span>  <span class="count-cart"> <?= $count_cart; ?> </span>
                            </a>
                            <div class="panel-collapse collapse" id="cart-collapse" role="tabpanel">
                                <div class="col-md-12">
                                    <?= $cart; ?>
                                </div>
                            </div>
                        </div>
                    </header>
                </div>
            </div>

            <!-- END NAVBAR -->
            <div class="clearfix"></div>
            <div class="col-md-12" id="filter-btn">
                <a class="toggle-sidebar">FILTRAR</a>
            </div>