<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:52
 */

require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'models/model_model.php';

class Home_model extends Model_model
{
    private $model;

    public function __construct()
    {
        $this->model = new Model_model();
    }

    /*
     * Método que retorna as categorias do navbar
     */
    public function getNavCategories(){
        $categories = array();

        // Busco as seções
        $secao = $this->model->select('secao', array('id','nome'),array('status' => 1));

        // Se retornar registros faço loop e busco as categorias relacionado a cada seção
        if(count($secao) > 0){
            foreach ($secao as $key => $val):
                $categories[$val->nome] = $this->model->query("
                    SELECT 
                        id, nome
                    FROM
                        categoria
                    WHERE
                        status = 1 AND
                        secao LIKE '%{$val->id}%'"
                );

            endforeach;

            return $categories;
        }
    }

    /*
     * Método que retorna as marcas
     */
    public function getBrands(){
        return $this->model->query('
            SELECT 
                m.id 	      as id,
                m.nome 	      as nome,
                count(p.nome) as total
            FROM
                marca m	
            LEFT JOIN
                produto p
                ON p.marca = m.id
            WHERE                 
                m.status = 1
            GROUP BY
                m.id              
        ');
    }


    /*
     * Método que retorna os produtos
     */
    public function getProducts($where = null){
        return $this->model->select('produto', array('id','nome','preco','foto'), $where);
    }

    /*
     * Método que retorna os produtos
     */
    public function getProductsPagination($where = null, $offSet = null, $limit = null){
        $condicao = '';
        // Verifico se $where veio do controller. Se sim e não estiver vazio, percorro o mesmo.
        if(is_array($where) && count($where) > 0):
            $i = 0;
            foreach($where as $key => $value):
                $i++;
                $condicao .= $key . ' = '. $value.((count($where) == $i) ? '' : ' AND ' );
            endforeach;
        else:
            $condicao = $where;
        endif;

        return $this->model->query("
            SELECT
                id,
                nome,
                preco,
                foto
            FROM
              produto
            WHERE
                $condicao
            LIMIT
              $offSet, $limit
        ");
    }
}