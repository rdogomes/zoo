<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:28
 */

include_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'controller/controller.php';

class Home extends Controller
{
    private $model = '';

    public function __construct()
    {
        $this->loadModel('site/models/home');
        $this->model = new Home_model();
    }

    public function index($filter){
        $data['categories'] = $this->model->getNavCategories();
        $data['brands']     = $this->model->getBrands();
        $data['cart']       = $this->loadCart();
        $data['count_cart'] = isset($_SESSION['carrinho']) ? count($_SESSION['carrinho']) : 0;

        // Retorna os produtos preparando também a paginação
        $limit = 9;
        $page = $filter ['page'];
        $offSet = ($page * $limit) - $limit;
        $count  = count($this->model->getProducts());
        $data['products']  = $this->model->getProductsPagination('status = 1', $offSet, $limit);
        $data['pagination'] = $this->pagination($limit, $page, $count, 'home/index/');

        $this->loadView('site/views/index', $data);
    }


    public function category($filter){
        $data['categories'] = $this->model->getNavCategories();
        $data['brands']     = $this->model->getBrands();
        $data['cart']       = $this->loadCart();
        $data['count_cart'] = isset($_SESSION['carrinho']) ? count($_SESSION['carrinho']) : 0;

        if(count($filter) > 0):
            $filter['status'] = 1;
            $page = $filter['page'];
            unset($filter['page']);
        endif;

        // Retorna os produtos preparando também a paginação
        $limit = 9;
        $offSet = ($page * $limit) - $limit;
        $count  = count($this->model->getProducts($filter));
        $data['products']  = $this->model->getProductsPagination($filter, $offSet, $limit);
        $data['pagination'] = $this->pagination($limit, $page, $count, 'c/'.$filter['categoria'].'/');

        $data['filter'] = $filter;
        //$data['products']   = $this->model->getProducts($filter);

        $this->loadView('site/views/index', $data);
    }

    public function brand($filter){
        $data['categories'] = $this->model->getNavCategories();
        $data['brands']     = $this->model->getBrands();
        $data['cart']       = $this->loadCart();
        $data['count_cart'] = isset($_SESSION['carrinho']) ? count($_SESSION['carrinho']) : 0;

        if(count($filter) > 0):
            $filter['status'] = 1;
            $page = $filter['page'];
            unset($filter['page']);
        endif;

        // Retorna os produtos preparando também a paginação
        $limit = 9;
        $offSet = ($page * $limit) - $limit;
        $count  = count($this->model->getProducts($filter));
        $data['products']  = $this->model->getProductsPagination($filter, $offSet, $limit);
        $data['pagination'] = $this->pagination($limit, $page, $count, 'm/'.$filter['marca'].'/');

        $data['filter'] = $filter;
        //$data['products']   = $this->model->getProducts($filter);

        $this->loadView('site/views/index', $data);
    }


    public function addToCart(){
        if(isset($_POST)):
            $post = $_POST;

            //unset($_SESSION['carrinho']);
            //exit();

            $produto = current($this->model->getProducts(array('id' => $post['id'])));

            if(count($produto) > 0):
                $_SESSION['carrinho'][$produto->id]['id'] 	 = $produto->id;
                $_SESSION['carrinho'][$produto->id]['nome']	 = $produto->nome;
                $_SESSION['carrinho'][$produto->id]['preco'] = $produto->preco;
                $_SESSION['carrinho'][$produto->id]['foto']  = $produto->foto;
                $_SESSION['carrinho'][$produto->id]['qtd'] 	 = 1;

                echo $this->loadCart();
            else:
                echo 'erro';
            endif;



        endif;
    }

    public function removeToCart(){
        if(isset($_POST)):
            $post = $_POST;

            // Remove do carrinho
            foreach ($_SESSION['carrinho'] as $key => $value):
                if ($key == $post['id']):
                    unset($_SESSION['carrinho'][$key]);
                    echo $this->loadCart();
                    $count = 0;
                    foreach ($_SESSION['carrinho'] as $value):
                        $count += $value['qtd'];
                    endforeach;
                    if($count == 0):
                        session_destroy();
                    endif;
                endif;
            endforeach;
        endif;
    }

    public function loadCart(){
        $cart = '';

        if(isset($_SESSION['carrinho']) && count($_SESSION) > 0):
            foreach ($_SESSION['carrinho']as $val):
                 $cart .= '
                    <div class="row line">
                        <div class="img">
                            <img src="'.URL_SITE.'assets/img/upload/'.$val['foto'].'" width="75" alt="">
                        </div>
                        <div class="info">
                            <p class="title">
                                '.$val['nome'].'<br>
                                QTY:'.$val['qtd'].'<span class="remove-cart" id="remove-'.$val['id'].'"> x </span> <br>
                                <strong>$ '.$val['preco'].'</strong>
                            </p>
                        </div>
                    </div>';
            endforeach;
        else:
            return "<strong>Vazio</strong>";
        endif;
        return $cart;
    }

}