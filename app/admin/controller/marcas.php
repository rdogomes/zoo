<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:28
 */
require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'controller/controller.php';

class Marcas extends Controller
{
    private $model = '';

    public function __construct()
    {
        // Carrega o model para a variavel model
        $this->loadModel('admin/models/marcas');
        $this->model = new Marcas_model();
    }

    /*
     * carrega a view de lista das Marcas
     */
    public function index(){

        $data['marcas'] = $this->model->getAll();

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/marcas/index', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que carrega a view de atualização
     */
    public function editar($id){
        if(isset($id)){
            $marca = $this->model->get($id);
            if(count($marca) > 0):
                $data['marca'] = current($marca);
            endif;
        }
        $data['id'] = $id;

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/marcas/form', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que carrega a view de cadastro
     */
    public function novo(){
        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/marcas/form');
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que cadastra e atualiza se houver a variavel $_POST['id']
     */
    public function salvar(){

        if(isset($_POST) && count($_POST) > 0):
            $post = $_POST;

            // Valido a variavel $_POST
            if($this->validation($post) == FALSE):
                // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                $_SESSION['msg'] = "Favor preencher todos os campos!";
                $this->redirect(URL_SITE.'admin/marcas/novo/');
                exit();
            endif;

            // Crio a variavel com os campos e valores
            $values = array(
                'nome'      => addslashes($post['nome']),
                'status'    => addslashes($post['status'])
            );

            // Verifico se existe a variavel $id. Se sim executo o método atualiza().
            // Se não executo o método insere()
            if(isset($post['id']) && $post['id'] > 0):

                $return = $this->model->atualiza($values, $post['id']);
                if($return != FALSE):
                    // Se gravou com sucesso crio a variavel msg com mensagem de sucesso
                    $_SESSION['msg'] = "Marca atualizada com sucesso!";
                    echo $_SESSION['msg'] ;
                    exit();
                    $this->redirect(URL_SITE.'admin/marcas/editar/'.$post['id']);
                else:
                    // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                    $_SESSION['msg'] = "Erro ao atualizar Marca!";
                    $this->redirect(URL_SITE.'admin/marcas/editar/'.$post['id']);
                endif;
            else:

                $lastId = $this->model->insere($values);

                if($lastId > 0):
                    // Se gravou com sucesso crio a variavel msg com mensagem de sucesso
                    $_SESSION['msg'] = "Marca cadastrada com sucesso!";
                    $this->redirect(URL_SITE.'admin/marcas/editar/'.$lastId);
                else:
                    // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                    $_SESSION['msg'] = "Erro ao cadastradar Marca!";
                    $this->redirect(URL_SITE.'admin/marcas/novo/');
                endif;
            endif;
        endif;

        //Se existir id busco os dados do mesmo
        if(isset($id)){
            $marca = $this->model->get($id);
        }

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/marcas/form');
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método para excluir Marcas.
     */
    public function excluir($id){
        // Verifico se não está vazio
        if($id != ''):
            // Executo o método para excluir a Marca
            if($this->model->exclui($id)):
                // Se excluir com sucesso crio a variavel msg com mensagem de sucesso
                $_SESSION['msg'] = "Marca excluída com sucesso!";
                $this->redirect(URL_SITE.'admin/marcas/index/');
            else:
                // Se não excluir com sucesso crio a variavel msg com mensagem de erro
                $_SESSION['msg'] = "Erro ao excluir Marca!";
                $this->redirect(URL_SITE.'admin/marcas/index/');
            endif;
        endif;
    }

}