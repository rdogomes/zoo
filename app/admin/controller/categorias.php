<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:28
 */
require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'controller/controller.php';

class Categorias extends Controller
{
    private $model = '';

    public function __construct()
    {
        // Carrega o model para a variavel model
        $this->loadModel('admin/models/categorias');
        $this->model = new Categorias_model();
    }

    /*
     * carrega a view de lista das categorias
     */
    public function index(){

        $data['categorias'] = $this->model->getAll();

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/categorias/index', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que carrega a view de atualização
     */
    public function editar($id){
        if(isset($id)){
            $categoria = $this->model->get($id);
            if(count($categoria) > 0):
                $data['categoria'] = current($categoria);
            endif;
        }
        $data['id'] = $id;

        $data['secao'] = $this->model->getSections();

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/categorias/form', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que carrega a view de cadastro
     */
    public function novo(){
        $data['secao'] = $this->model->getSections();

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/categorias/form', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que cadastra e atualiza se houver a variavel $_POST['id']
     */
    public function salvar(){

        if(isset($_POST) && count($_POST) > 0):
            $post = $_POST;

            // Valido a variavel $_POST
            if($this->validation($post) == FALSE):
                // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                $_SESSION['msg'] = "Favor preencher todos os campos!";
                $this->redirect(URL_SITE.'admin/categorias/novo/');
                exit();
            endif;

            // Crio a variavel com os campos e valores
            $values = array(
                'nome'      => addslashes($post['nome']),
                'secao'     => addslashes( ((is_array($post['secao']) && count($post['secao']) > 0) ? implode(',', $post['secao']) : $post['secao']) ),
                'status'    => addslashes($post['status'])
            );

            // Verifico se existe a variavel $id. Se sim executo o método atualiza().
            // Se não executo o método insere()
            if(isset($post['id']) && $post['id'] > 0):

                $return = $this->model->atualiza($values, $post['id']);
                if($return != FALSE):
                    // Se gravou com sucesso crio a variavel msg com mensagem de sucesso
                    $_SESSION['msg'] = "Categoria atualizada com sucesso!";
                    $this->redirect(URL_SITE.'admin/categorias/editar/'.$post['id']);
                else:
                    // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                    $_SESSION['msg'] = "Erro ao atualizar categoria!";
                    $this->redirect(URL_SITE.'admin/categorias/editar/'.$post['id']);
                endif;
            else:

                $lastId = $this->model->insere($values);

                if($lastId > 0):
                    // Se gravou com sucesso crio a variavel msg com mensagem de sucesso
                    $_SESSION['msg'] = "Categoria cadastrada com sucesso!";
                    $this->redirect(URL_SITE.'admin/categorias/editar/'.$lastId);
                else:
                    // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                    $_SESSION['msg'] = "Erro ao cadastradar categoria!";
                    $this->redirect(URL_SITE.'admin/categorias/novo/');
                endif;
            endif;
        endif;

        //Se existir id busco os dados do mesmo
        if(isset($id)){
            $categoria = $this->model->get($id);
        }

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/categorias/form');
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método para excluir categorias.
     */
    public function excluir($id){
        // Verifico se não está vazio
        if($id != ''):
            // Executo o método para excluir a categoria
            if($this->model->exclui($id)):
                // Se excluir com sucesso crio a variavel msg com mensagem de sucesso
                $_SESSION['msg'] = "Categoria excluída com sucesso!";
                $this->redirect(URL_SITE.'admin/categorias/index/');
            else:
                // Se não excluir com sucesso crio a variavel msg com mensagem de erro
                $_SESSION['msg'] = "Erro ao excluir categoria!";
                $this->redirect(URL_SITE.'admin/categorias/index/');
            endif;
        endif;
    }

}