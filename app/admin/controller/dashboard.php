<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:28
 */
require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'controller/controller.php';

class Dashboard extends Controller
{
    public function __construct()
    {
    }

    public function index(){
        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/dashboard/index');
        $this->loadView('admin/views/default/footer');
    }
}