<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:28
 */
require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'controller/controller.php';

class Secao extends Controller
{
    private $model = '';

    public function __construct()
    {
        // Carrega o model para a variavel model
        $this->loadModel('admin/models/secao');
        $this->model = new Secao_model();
    }

    /*
     * carrega a view de lista das Seção
     */
    public function index(){

        $data['secoes'] = $this->model->getAll();

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/secao/index', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que carrega a view de atualização
     */
    public function editar($id){
        if(isset($id)){
            $secao = $this->model->get($id);
            if(count($secao) > 0):
                $data['secao'] = current($secao);
            endif;
        }
        $data['id'] = $id;

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/secao/form', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que carrega a view de cadastro
     */
    public function novo(){
        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/secao/form');
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que cadastra e atualiza se houver a variavel $_POST['id']
     */
    public function salvar(){

        if(isset($_POST) && count($_POST) > 0):
            $post = $_POST;

            // Valido a variavel $_POST
            if($this->validation($post) == FALSE):
                // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                $_SESSION['msg'] = "Favor preencher todos os campos!";
                $this->redirect(URL_SITE.'admin/secao/novo/');
                exit();
            endif;

            // Crio a variavel com os campos e valores
            $values = array(
                'nome'      => addslashes($post['nome']),
                'status'    => addslashes($post['status'])
            );

            // Verifico se existe a variavel $id. Se sim executo o método atualiza().
            // Se não executo o método insere()
            if(isset($post['id']) && $post['id'] > 0):

                $return = $this->model->atualiza($values, $post['id']);
                if($return != FALSE):
                    // Se gravou com sucesso crio a variavel msg com mensagem de sucesso
                    $_SESSION['msg'] = "Seção atualizada com sucesso!";
                    $this->redirect(URL_SITE.'admin/secao/editar/'.$post['id']);
                else:
                    // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                    $_SESSION['msg'] = "Erro ao atualizar Seção!";
                    $this->redirect(URL_SITE.'admin/secao/editar/'.$post['id']);
                endif;
            else:

                $lastId = $this->model->insere($values);

                if($lastId > 0):
                    // Se gravou com sucesso crio a variavel msg com mensagem de sucesso
                    $_SESSION['msg'] = "Seção cadastrada com sucesso!";
                    $this->redirect(URL_SITE.'admin/secao/editar/'.$lastId);
                else:
                    // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                    $_SESSION['msg'] = "Erro ao cadastradar Seção!";
                    $this->redirect(URL_SITE.'admin/secao/novo/');
                endif;
            endif;
        endif;

        //Se existir id busco os dados do mesmo
        if(isset($id)){
            $secao = $this->model->get($id);
        }

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/marcas/form');
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método para excluir Seção.
     */
    public function excluir($id){
        // Verifico se não está vazio
        if($id != ''):
            // Executo o método para excluir a Seção
            if($this->model->exclui($id)):
                // Se excluir com sucesso crio a variavel msg com mensagem de sucesso
                $_SESSION['msg'] = "Seção excluída com sucesso!";
                $this->redirect(URL_SITE.'admin/secao/index/');
            else:
                // Se não excluir com sucesso crio a variavel msg com mensagem de erro
                $_SESSION['msg'] = "Erro ao excluir Seção!";
                $this->redirect(URL_SITE.'admin/secao/index/');
            endif;
        endif;
    }

}