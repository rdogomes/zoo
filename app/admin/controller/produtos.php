<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:28
 */
require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'controller/controller.php';

class Produtos extends Controller
{
    private $model;

    public function __construct()
    {
        $this->loadModel('admin/models/produtos');
        $this->model = new Produtos_model();
    }

    /*
      * carrega a view de lista das categorias
      */
    public function index(){

        $data['produtos'] = $this->model->getAll();

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/produtos/index', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que carrega a view de atualização
     */
    public function editar($id){
        if(isset($id)){
            $produto = $this->model->get($id);
            if(count($produto) > 0):
                $data['produto'] = current($produto);
            endif;
        }
        $data['id'] = $id;

        $data['categories'] = $this->model->getCategories();
        $data['brands']     = $this->model->getBrands();

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/produtos/form', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que carrega a view de cadastro
     */
    public function novo(){
        $data['categories'] = $this->model->getCategories();
        $data['brands']     = $this->model->getBrands();

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/produtos/form', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método que cadastra e atualiza se houver a variavel $_POST['id']
     */
    public function salvar(){

        if(isset($_POST) && count($_POST) > 0):
            $post = $_POST;

            // Valido a variavel $_POST
            if($this->validation($post) == FALSE):
                // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                $_SESSION['msg'] = "Favor preencher todos os campos!";
                $this->redirect(URL_SITE.'admin/produtos/novo/');
                exit();
            endif;

            // Crio a variavel com os campos e valores
            $values = array(
                'foto'      => addslashes($post['imagem']),
                'nome'      => addslashes($post['nome']),
                'marca'     => addslashes((int)$post['marca']),
                'preco'     => addslashes($post['preco']),
                'categoria' => addslashes((int)$post['categoria']),
                'status'    => addslashes($post['status'])
            );

            // Se foi incluído alguma foto, faço o upload.
            if(isset($_FILES['foto']) && $_FILES['foto']['name'] != ''):
                $values['foto'] = $this->uploadImage($_FILES['foto'], 'assets/img/upload/');
            endif;


            // Verifico se existe a variavel $id. Se sim executo o método atualiza().
            // Se não executo o método insere()
            if(isset($post['id']) && $post['id'] > 0):
                $return = $this->model->atualiza($values, $post['id']);
                if($return != FALSE):
                    // Se gravou com sucesso crio a variavel msg com mensagem de sucesso
                    $_SESSION['msg'] = "Produto atualizado com sucesso!";
                    $this->redirect(URL_SITE.'admin/produtos/editar/'.$post['id']);
                else:
                    // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                    $_SESSION['msg'] = "Erro ao atualizar produto!";
                    $this->redirect(URL_SITE.'admin/produtos/editar/'.$post['id']);
                endif;
            else:
                print_r($values);
                $lastId = $this->model->insere($values);

                if($lastId > 0):
                    // Se gravou com sucesso crio a variavel msg com mensagem de sucesso
                    $_SESSION['msg'] = "Produto cadastrado com sucesso!";
                    $this->redirect(URL_SITE.'admin/produtos/editar/'.$lastId);
                else:
                    // Se não gravou com sucesso crio a variavel msg com mensagem de erro
                    $_SESSION['msg'] = "Erro ao cadastradar produto!";
                    $this->redirect(URL_SITE.'admin/produtos/novo/');
                endif;
            endif;
        endif;

        //Se existir id busco os dados do mesmo
        if(isset($id)){
            $data['produto'] = $this->model->get($id);
        }

        $this->loadView('admin/views/default/header');
        $this->loadView('admin/views/produtos/form', $data);
        $this->loadView('admin/views/default/footer');
    }

    /*
     * Método para excluir produtos.
     */
    public function excluir($id){
        // Verifico se não está vazio
        if($id != ''):
            // Executo o método para excluir o produto
            if($this->model->exclui($id)):
                // Se excluir com sucesso crio a variavel msg com mensagem de sucesso
                $_SESSION['msg'] = "Produto excluído com sucesso!";
                $this->redirect(URL_SITE.'admin/produtos/index/');
            else:
                // Se não excluir com sucesso crio a variavel msg com mensagem de erro
                $_SESSION['msg'] = "Erro ao excluir produto!";
                $this->redirect(URL_SITE.'admin/produtos/index/');
            endif;
        endif;
    }


}