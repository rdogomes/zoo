<div class="container">
    <section id="list">
        <div class="col-md-12">
            <!-- EXIBO MENSAGEM DE RETORNO SE HOUVER -->
            <?php if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''): ?>
                <div class="alert alert-success" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?= $_SESSION['msg']; ?>
                    <?php unset($_SESSION['msg']); ?>
                </div>
            <?php endif; ?>

            <?php if(isset($marcas) && count($marcas) > 0): ?>
                <table class="table table-bordered table-hover table-striped table-responsive">
                    <thead>
                    <tr class="text-center">
                        <td>#</td>
                        <td>Nome</td>
                        <td>Status</td>
                        <td>Ação</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($marcas as $row): ?>
                        <tr>
                            <td class="text-center"><?= $row->id; ?></td>
                            <td><?= $row->nome; ?></td>
                            <td class="text-center"><?= ($row->status == 1) ? 'ATIVO' : 'INATIVO'; ?></td>
                            <td  class="text-center">
                                <a href="<?= URL_SITE; ?>admin/marcas/editar/<?= $row->id; ?>/"><i class="fa fa-edit"></i> Editar</a><br>
                                <a href="<?= URL_SITE; ?>admin/marcas/excluir/<?= $row->id; ?>/"><i class="fa fa-trash"></i> Excluir</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <div class="alert alert-danger" role="alert">
                    <h3>Nenhuma marca cadastrada! <a href="<?= URL_SITE; ?>admin/marcas/novo/"> Clique aqui para cadastrar uma.</a></h3>
                </div>
            <?php endif; ?>

            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-end">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>
</div>

