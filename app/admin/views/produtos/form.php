<section id="form">
    <form action="<?= URL_SITE.'admin/produtos/salvar'; ?>" method="post" enctype="multipart/form-data">

        <?php if(isset($id)): ?>
            <input type="hidden" name="id" value="<?= $id; ?>">
        <?php endif; ?>
        <div class="container">
            <!-- EXIBO MENSAGEM DE RETORNO SE HOUVER -->
            <?php if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''): ?>
                <?= $this->alert($_SESSION['msg'],''); ?>
                <?php unset($_SESSION['msg']); ?>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-3 offset-md-1">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div id="image-holder">
                                <img src="<?= (isset($produto) && count($produto) > 0) ? URL_SITE.'assets/img/upload/' . $produto->foto : URL_SITE.'assets/img/no-image.png'; ?>" width="225" class="pull-left" id="thumb">
                            </div>

                            <span class="upload-legend">clique acima para escolher uma imagem</span>
                            <br>
                            <input type="file" name="foto" id="foto" class="form-control text-hide" style="display:none">
                            <input type="text" name="imagem" id="imagem" class="form-control" value="<?= (isset($produto) && count($produto) > 0) ? $produto->foto : ''; ?>" placeholder="Foto">
                        </div>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <input type="text" name="nome" id="nome" class="form-control" value="<?= isset($produto) ? $produto->nome : null; ?>" placeholder="Nome" autofocus>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="marca" class="form-control">
                                    <option value="">MARCA</option>
                                    <?php if(isset($brands) && count($brands) > 0): ?>
                                        <?php foreach ($brands as $value): ?>
                                            <option value="<?= $value->id; ?>" <?= (isset($produto) && $produto->marca == $value->id) ? 'selected' : ''; ?>><?= $value->nome; ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" name="preco" id="preco" class="form-control" value="<?= isset($produto) ? $produto->preco : null; ?>" placeholder="Preço" autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <select name="categoria" class="form-control">
                                    <option value="">CATEGORIA</option>
                                    <?php if(isset($categories) && count($categories) > 0): ?>
                                        <?php foreach ($categories as $value): ?>
                                            <option value="<?= $value->id; ?>" <?= (isset($produto) && $produto->categoria == $value->id) ? 'selected' : ''; ?>><?= $value->nome; ?></option>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="status" class="form-control">
                                    <option value="">STATUS</option>
                                    <option value="1" <?= isset($produto) && $produto->status == 1 ? 'selected' : ''; ?>>ATIVO</option>
                                    <option value="0" <?= isset($produto) && $produto->status == 0 ? 'selected' : ''; ?>>INATIVO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <button class="btn btn-success pull-4 float-right">SALVAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>

</section>