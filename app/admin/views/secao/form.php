<section id="form">
    <form action="<?= URL_SITE.'admin/secao/salvar'; ?>" method="post">
        <?php if(isset($id)): ?>
            <input type="hidden" name="id" value="<?= $id; ?>">
        <?php endif; ?>
        <div class="container">
            <!-- EXIBO MENSAGEM DE RETORNO SE HOUVER -->
            <?php if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''): ?>
                <?= $this->alert($_SESSION['msg'],''); ?>
                <?php unset($_SESSION['msg']); ?>
            <?php endif; ?>
            <div class="row">
                <div class="col-md-6 offset-3">
                    <div class="form-group">
                        <input type="text" name="nome" id="nome" class="form-control" value="<?= isset($secao) ? $secao->nome : null; ?>" placeholder="Nome" autofocus>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 offset-3">
                    <div class="form-group">
                        <select name="status" class="form-control">
                            <option value="">STATUS</option>
                            <option value="1" <?= isset($secao) && $secao->status == 1 ? 'selected' : ''; ?>>SIM</option>
                            <option value="0" <?= isset($secao) && $secao->status == 0 ? 'selected' : ''; ?>>NÃO</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 offset-3">
                    <div class="form-group">
                        <button class="btn btn-success pull-4 float-right">SALVAR</button>
                    </div>
                </div>
            </div>
        </div>

    </form>

</section>


