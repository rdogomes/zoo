<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Categorias</title>
    <link href="<?= URL_SITE; ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= URL_SITE; ?>assets/css/font-awesome.css">
    <link href="<?= URL_SITE; ?>assets/css/admin-style.css" rel="stylesheet">

    <link href="<?= URL_SITE; ?>assets/library/chosen/chosen.css" rel="stylesheet">


</head>
<body>

<header>
    <div class="container-fluid">
        <nav class="navbar navbar-toggleable-md navbar-light bg-primary">
            <div class="container">
                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="<?= URL_SITE; ?>admin/"><img src="<?= URL_SITE; ?>assets/img/logo.png" class="logo-admin" alt="Zoo" title="Zoo"> </a>
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="catdropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Categorias</a>
                            <div class="dropdown-menu" aria-labelledby="catdropdown" >
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/categorias/index/"><i class="fa fa-list-alt"></i> Lista</a>
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/categorias/novo/"><i class="fa fa-plus-circle"></i> Novo</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="marcadropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Marcas</a>
                            <div class="dropdown-menu" aria-labelledby="marcadropdown" >
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/marcas/index/"><i class="fa fa-list-alt"></i> Lista</a>
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/marcas/novo/"><i class="fa fa-plus-circle"></i> Novo</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="marcadropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Seção</a>
                            <div class="dropdown-menu" aria-labelledby="marcadropdown" >
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/secao/index/"><i class="fa fa-list-alt"></i> Lista</a>
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/secao/novo/"><i class="fa fa-plus-circle"></i> Novo</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="produtodropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Produtos</a>
                            <div class="dropdown-menu" aria-labelledby="produtodropdown" >
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/produtos/index/"><i class="fa fa-list-alt"></i> Lista</a>
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/produtos/novo/"><i class="fa fa-plus-circle"></i> Novo</a>
                            </div>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="marcadropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-user-circle"></i> Perfil
                            </a>
                            <div class="dropdown-menu" aria-labelledby="marcadropdown" >
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/perfil/editar/"><i class="fa fa-edit"></i> Editar</a>
                                <a class="dropdown-item" href="<?= URL_SITE; ?>admin/perfil/sair/"><i class="fa fa-sign-out"></i> Sair</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </nav>
    </div>
</header>
<div class="clearfix"></div>