    <div class="clearfix"></div>
    <footer class="footer">
        <div class="container">
            <span class="text-muted"><i class="fa fa-copyright"></i> Rodigo Gomes - Todos os direitos reservados. Development by Rodrigo Gomes</span>
        </div>
    </footer>

    <!-- Scrips includes -->
    <script src="<?= URL_SITE; ?>assets/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="<?= URL_SITE; ?>assets/js/tether.min.js"></script>
    <script src="<?= URL_SITE; ?>assets/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= URL_SITE; ?>assets/library/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?= URL_SITE; ?>assets/js/admin-script.js" type="text/javascript"></script>

</body>
</html>