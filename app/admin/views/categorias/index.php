    <div class="container">
        <section id="list">
            <div class="col-md-12">
                <!-- EXIBO MENSAGEM DE RETORNO SE HOUVER -->
                <?php if(isset($_SESSION['msg']) && $_SESSION['msg'] != ''): ?>
                    <?= $this->alert($_SESSION['msg']); ?>
                    <?php unset($_SESSION['msg']); ?>
                <?php endif; ?>

                <?php if(isset($categorias) && count($categorias) > 0): ?>
                    <table class="table table-bordered table-hover table-striped table-responsive">
                        <thead>
                        <tr class="text-center">
                            <td>#</td>
                            <td>Nome</td>
                            <td>Status</td>
                            <td>Ação</td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($categorias as $row): ?>
                            <tr>
                                <td class="text-center"><?= $row->id; ?></td>
                                <td><?= $row->nome; ?></td>
                                <td class="text-center"><?= ($row->status == 1) ? 'ATIVO' : 'INATIVO'; ?></td>
                                <td  class="text-center">
                                    <a href="<?= URL_SITE; ?>admin/categorias/editar/<?= $row->id; ?>/"><i class="fa fa-edit"></i> Editar</a><br>
                                    <a href="<?= URL_SITE; ?>admin/categorias/excluir/<?= $row->id; ?>/"><i class="fa fa-trash"></i> Excluir</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <div class="alert alert-danger" role="alert">
                        <h3>Nenhuma categoria cadastrada! <a href="<?= URL_SITE; ?>admin/categorias/novo/"> Clique aqui para cadastrar uma.</a></h3>
                    </div>
                <?php endif; ?>
            </div>
        </section>
    </div>

