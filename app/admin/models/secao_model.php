<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:52
 */

require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'models/model_model.php';

class Secao_model extends Model_model
{
    private $model = '';
    public $tabela = 'secao';

    public function __construct()
    {
        $this->model = new Model_model();
    }

    /*
     * Método que retorna os dados da seção passada por parâmetro
     */
    public function get($id){
        return $this->model->select(
            $this->tabela,
            '*',
            array(
                'id' => $id
            )
        );
    }

    /*
     * Método que retorna os dados de todas as seção
     */
    public function getAll(){
        return $this->model->select(
            $this->tabela,
            '*'
        );
    }

    /*
     * Método que cadastra uma nova seção
     */
    public function insere($data){
        if(count($data) > 0):
            return $this->model->insert($this->tabela, $data);
        endif;

        return FALSE;
    }

    /*
     * Método que atualiza uma seção
     */
    public function atualiza($data, $id){
        if(count($data) > 0):
            return $this->model->update($this->tabela, $data, $id);
        endif;

        return FALSE;
    }

    /*
     * Método que exclui seção passada por parâmetro
     */
    public function exclui($id){
        if($id != ''):
            return $this->model->delete($this->tabela, array('id' => $id));
        endif;
    }
}