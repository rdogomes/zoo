<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:52
 */

require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'models/model_model.php';

class Produtos_model extends Model_model
{
    private $model = '';
    public $tabela = 'produto';

    public function __construct()
    {
        $this->model = new Model_model();
    }

    /*
     * Método que retorna os dados do produto passado por parâmetro
     */
    public function get($id){
        return $this->model->select(
            $this->tabela,
            '*',
            array(
                'id' => $id
            )
        );
    }

    /*
     * Método que retorna os dados de todos os produtos
     */
    public function getAll(){
        return $this->model->select(
            $this->tabela,
            '*'
        );
    }

    /*
     * Método que retorna os dados de todas as marcas
     */
    public function getBrands(){
        return $this->model->select(
            'marca',
            '*',
            array('status' => 1)
        );
    }

    /*
     * Método que retorna os dados de todas as categorias
     */
    public function getCategories(){
        return $this->model->select(
            'categoria',
            '*',
            array('status' => 1)
        );
    }

    /*
     * Método que cadastra um novo produto
     */
    public function insere($data){
        if(count($data) > 0):
            return $this->model->insert($this->tabela, $data);
        endif;

        return FALSE;
    }

    /*
     * Método que atualiza um produto
     */
    public function atualiza($data, $id){
        if(count($data) > 0):
            return $this->model->update($this->tabela, $data, $id);
        endif;

        return FALSE;
    }

    /*
     * Método que exclui produto passado por parâmetro
     */
    public function exclui($id){
        if($id != ''):
            return $this->model->delete($this->tabela, array('id' => $id));
        endif;
    }
}