<?php

/**
 * Created by Rodrigo Gomes do Nascimento.
 * Date: 13/01/2017
 * Time: 16:52
 */

require_once SYSTEM_FOLDER . DIRECTORY_SEPARATOR . 'models/model_model.php';

class Categorias_model extends Model_model
{
    private $model = '';
    public $tabela = 'categoria';

    public function __construct()
    {
        $this->model = new Model_model();
    }

    /*
     * Método que retorna os dados da categoria passada por parâmetro
     */
    public function get($id){
        return $this->model->select(
            $this->tabela,
            '*',
            array(
                'id' => $id
            )
        );
    }

    /*
     * Método que retorna os dados de todas as categoria
     */
    public function getAll(){
        return $this->model->select(
            $this->tabela,
            '*'
        );
    }

    /*
     * Método que retorna os dados de todas as seções
     */
    public function getSections(){
        return $this->model->select(
            'secao',
            '*'
        );
    }

    /*
     * Método que cadastra uma nova categoria
     */
    public function insere($data){
        if(count($data) > 0):
            return $this->model->insert($this->tabela, $data);
        endif;

        return FALSE;
    }

    /*
     * Método que atualiza uma nova categoria
     */
    public function atualiza($data, $id){
        if(count($data) > 0):
            return $this->model->update($this->tabela, $data, $id);
        endif;

        return FALSE;
    }

    /*
     * Método que exclui categoria passada por parâmetro
     */
    public function exclui($id){
        if($id != ''):
            return $this->model->delete($this->tabela, array('id' => $id));
        endif;
    }
}