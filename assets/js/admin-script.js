/* Created by Rodrigo Gomes do Nascimento on 18/01/2017. */

$(document).ready(function(){
    /****** Simula click no input file no form de produtos *********/
    $('#thumb').on('click', function(){
        $('#foto').trigger('click');

        // Exibe a imagem
        $('#foto').on('change', function(){
            $('#imagem').val($(this).val());
            if (typeof (FileReader) != "undefined") {
                var image_holder = $("#image-holder");
                image_holder.empty();

                var reader = new FileReader();
                reader.onload = function (e) {
                    $("<img />", {
                        "src": e.target.result,
                        "class": "thumb-image",
                        "width": 225
                    }).appendTo(image_holder);
                }
                image_holder.show();
                reader.readAsDataURL($(this)[0].files[0]);
            } else{
                alert("Este navegador nao suporta FileReader.");
            }
        });
    });

    /********* Chosen Selects *************/
    $(".chosen-select").chosen()

})




