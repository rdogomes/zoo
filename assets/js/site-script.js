/*
 * Created by Rodrigo Gomes do Nascimento on 20/01/2017.
 */

$(document).ready(function(){
    $('.cart-icon').on('click', function(){
        var sp = $(this).attr('id').split('-');

        // ADICIONA O PRODUTO AO CARRINHO E ATUALIZA O HTML DO MESMO
        $.ajax({
            type: 'POST',
            data: {id: sp[1]},
            url: '<?= URL_SITE; ?>app/site/home/addToCart/',
            success: function (data) {
                alert(data)
            }
        });
    });
});
